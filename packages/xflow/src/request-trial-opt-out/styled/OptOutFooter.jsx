import styled from 'styled-components';

import { gridSize } from '@atlaskit/theme';

const OptOutFooter = styled.div`
  margin-bottom: ${gridSize}px;
  margin-right: ${gridSize}px;
  text-align: right;
`;

OptOutFooter.displayName = 'OptOutFooter';
export default OptOutFooter;
