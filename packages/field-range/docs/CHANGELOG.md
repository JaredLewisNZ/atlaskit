# @atlaskit/field-range

## 2.4.5 (2017-11-15)

* bug fix; bumping internal dependencies to latest major version ([7857613](https://bitbucket.org/atlassian/atlaskit/commits/7857613))
## 2.4.4 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))
## 2.4.3 (2017-10-22)

* bug fix; update styled component dependency and react peerDep ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))














## 2.4.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 2.4.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 2.1.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 1.0.0 (2017-07-06)


* feature; convert Slider component into fieldRange one ([f5dc3dd](https://bitbucket.org/atlassian/atlaskit/commits/f5dc3dd))
* feature; update FieldRange specs to make it compatible with IE ([25963aa](https://bitbucket.org/atlassian/atlaskit/commits/25963aa))
* feature; update naming from sliderRange docs ([41c3af1](https://bitbucket.org/atlassian/atlaskit/commits/41c3af1))
